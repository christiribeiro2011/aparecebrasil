<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{
    //

	public function index()
	{
		return view('index');
	}

	 public function send(Request $request)
    {
         
         	$data = array(
              'nome' => $request->nome,
              'email' => $request->email,
              'mensagem' => $request->mensagem,
            );

            Mail::send('email.send', $data, function($message) use ($data) {
              $message->from($data['email']);
              $message->to('christiribeiro2011@gmai.com');
              //$message->cc('andreia@agenciasisters.com.br');
              $message->subject("Aparece Brasil - Contato");
            });


                return redirect('/')->with('success', 'E-mail enviado com sucesso.');
    }

}
